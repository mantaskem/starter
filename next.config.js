const path = require("path");
const withTypescript = require("@zeit/next-typescript");
const withCSS = require("@zeit/next-css");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const withCustomBabelConfigFile = require("next-plugin-custom-babel-config");
const TsConfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

module.exports = withTypescript(
  withCSS({
    target: "serverless",
    withCustomBabelConfigFile: path.resolve("../babel.config.js"),
    webpack(config) {
      if (process.env.ANALYZE) {
        config.plugins.push(
          new BundleAnalyzerPlugin({
            analyzerMode: "server",
            analyzerPort: 8888,
            openAnalyzer: true
          })
        );
      }
      config.resolve = config.resolve || {};
      config.resolve.plugins = config.resolve.plugins || [];
      config.resolve.plugins.push(
        new TsConfigPathsPlugin({
          configFile: "./tsconfig.json"
        })
      );

      return config;
    },
    cssModules: true
  })
);
