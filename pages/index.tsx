import * as React from "react";
import SimpleButton from "components/simpleButton/Container";
import HiddenButton from "components/hiddenButton/Container";
import Header from "components/header/Component";
import Footer from "components/footer/Component";
import Layout from "components/layout/Component";

const LandingPage: React.FC = () => (
  <div className="w100p h100p t0 l0">
    <Header />
    <Layout>
      <h1>Landing page</h1>
      <div className="w30p">
        <SimpleButton />
        <HiddenButton />
      </div>
    </Layout>
    <Footer />
  </div>
);

export default LandingPage;
