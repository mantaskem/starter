import { connect } from "react-redux";
import { AppState } from "store/reducer";
import HiddenButton from "components/hiddenButton/Component";
import { increaseCount } from "features/simpleCounter/thunks";
import { withVisibility } from "hof/withVisibility";

const mapStateToPros = (state: AppState) => {
  return {
    isVisible: false,
    text: "I`m hidden!"
  };
};

const mapDispatchToProps = {
  handleClick: increaseCount
};

export default connect(
  mapStateToPros,
  mapDispatchToProps
)(withVisibility(HiddenButton));
