import * as React from 'react';

interface Props {
  text: string;
  handleClick: () => void;
}

const HiddenButton: React.FC<Props> = ({ text, handleClick }) => (
  <button onClick={handleClick}>
    {text}
  </button>
);

export default HiddenButton;
