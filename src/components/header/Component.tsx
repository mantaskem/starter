import * as React from "react";
// import { Link } from 'react-router-dom';
import { Button, ButtonSize, ButtonType } from "ui/components";

export const HEADER_HEIGHT = "10p";

const log = () => console.log("click");

const Header: React.FC = () => (
  <div className="t0 l0 df w100p posf jcsb bg-white" style={{ zIndex: 100 }}>
    <div className="pl10p pr10p w100p">
      <div
        style={{ borderBottom: "solid 1px #c0c0c0" }}
        className="w100p df aic h56"
      >
        <div className="w70p curp">Logo</div>
        <div className="w30p df jcsb aic">
          {/* <Link to="/" className="c-black">
            Home
          </Link>
          <Link to="/list" className="c-black">
            List
          </Link> */}
          <div className="w40p">
            <Button
              onClick={log}
              sizeType={ButtonSize.small}
              styleType={ButtonType.smallSecondary}
              style={{ borderRadius: 20 }}
            >
              Sign up
            </Button>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Header;
