import { merge } from 'ramda';

export interface AppConfig {
  evn: string;
  host: string;
  port: number;
  isDev: boolean;
  apiUrl: string;
  socketUrl: string;
  isBrowser: boolean;
  translateUrl: string;
}

// tslint:disable: no-string-literal

const defaultConfig = {
  common: {
    env: process.env.NODE_ENV || 'development',
    isDev: process.env.NODE_ENV !== 'production',
    isBrowser: process && process['browser'],
    basename: '/'
  },
  development: {
    host: 'localhost',
    port: process.env.DEV_PORT || 3001,
    apiUrl: `https://api.com/`
  },
  production: {
    host: 'localhost',
    port: 3000,
    apiUrl: `https://api.com/`
  }
};

export const config: AppConfig = merge(
  defaultConfig.common,
  defaultConfig[defaultConfig.common.env]
);
