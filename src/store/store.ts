import { applyMiddleware, compose, createStore, Store } from "redux";
import thunk from "redux-thunk";
import { persistStore, autoRehydrate } from "redux-persist";
import { AppState, reducer } from "store/reducer";

let store;

export const getStore = (state, isServer?): Store<AppState> => {
  if (isServer && typeof window === "undefined") {
    return createStore<AppState, any, {}, undefined>(
      reducer,
      state,
      applyMiddleware(thunk)
    );
  } else {
    const composeEnhancers =
      (true && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

    if (!store) {
      const mw = [thunk];
      if (window.__REACT_DEVTOOLS_GLOBAL_HOOK__) {
        window.__REACT_DEVTOOLS_GLOBAL_HOOK__.inject = function() {};
      }

      store = createStore<AppState, any, {}, undefined>(
        reducer,
        state,
        composeEnhancers(applyMiddleware(...mw), autoRehydrate())
      );

      const whitelist = ["persist"];
      persistStore(store, { whitelist }, _ => {
        console.log(`define whitelist: ${whitelist.join(", ")}`);
      });
    }
    return store;
  }
};
