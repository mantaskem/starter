import { combineReducers } from "redux";
import CounterReducer from "features/simpleCounter/reducer";
import { DeviceState } from "features/device/types";
import DeviceReducer from "features/device/reducer";
import UserReducer, { UserState } from "features/user/reducer";
import PageReducer, { PageState } from "features/page/reducer";

export interface AppState {
  counter: any;
  device: DeviceState;
  user: UserState;
  page: PageState;
}

export const reducer = combineReducers<AppState>({
  counter: CounterReducer,
  device: DeviceReducer,
  user: UserReducer,
  page: PageReducer
});
