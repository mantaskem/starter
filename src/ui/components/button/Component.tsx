import * as classnames from "classnames";
import * as React from "react";

export enum ButtonType {
  primary,
  secondary,
  smallSecondary,
  success,
  error,
  warning
}

export enum ButtonSize {
  large,
  medium,
  small,
  tiny
}

interface Props {
  styleType: ButtonType;
  sizeType: ButtonSize;
  className?: string;
  style?: React.CSSProperties;
  onClick?: () => void;
}

const ButtonSizeStyle = {
  [ButtonSize.large]: "fwb fz30 h100",
  [ButtonSize.medium]: "fw500 fz20 h60",
  [ButtonSize.small]: "fw600 fz14 h33"
};

const ButtonStyle = {
  [ButtonType.primary]: "c-white bg-primary",
  [ButtonType.secondary]:
    "bg-white bdc-secondary bdw3 c-secondary bg-secondary-h c-white-h",
  [ButtonType.smallSecondary]:
    "bg-white bdc-secondary bdw1 c-secondary bg-secondary-h c-white-h",
  [ButtonType.success]: "c-white bg-success bdw0",
  [ButtonType.warning]: "c-white bg-warning bdw0",
  [ButtonType.error]: "c-white bg-error bdw0"
};

export const Button: React.FC<Props> = ({
  children,
  styleType,
  sizeType,
  onClick,
  className,
  style
}) => {
  return (
    <button
      className={classnames(
        "btn ttu w100p bs-dark-grey",
        className,
        ButtonStyle[styleType],
        ButtonSizeStyle[sizeType]
      )}
      style={style}
      onClick={onClick}
    >
      {children}
    </button>
  );
};
