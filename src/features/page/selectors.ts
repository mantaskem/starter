import { AppState } from 'store/reducer';
import { PATHS } from './constants';

export const isLandingPage = (state: AppState) =>
  state.router.location.pathname === PATHS.LANDING_PAGE;

export const isListPage = (state: AppState) => state.router.location.pathname === PATHS.LIST_PAGE;
