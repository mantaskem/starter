import { Dispatch } from 'redux';
import { AppState } from 'store/reducer';
import {
  createBreakpointListener,
  createNetworkConnectionListener
} from 'features/device/listeners';
import { onPageUnload, onPageHidden, onPageVisible } from 'features/page/thunks';

export const initializeListeners = () => (dispatch: Dispatch<AppState>) => {
  dispatch(createBreakpointListener());
  dispatch(createNetworkConnectionListener());
  dispatch(initializeUnloadListener());
  dispatch(pageVisibilityListener());
};

const initializeUnloadListener = () => (dispatch: Dispatch<AppState>) => {
  window.addEventListener('beforeunload', () => dispatch(onPageUnload()));
};

const pageVisibilityListener = () => (dispatch: Dispatch<AppState>) => {
  document.addEventListener('visibilitychange', () => dispatch(handleVisibilityChange()));
};

const handleVisibilityChange = () => (dispatch: Dispatch<AppState>) => {
  if (document.hidden) {
    dispatch(onPageHidden());
  } else {
    dispatch(onPageVisible());
  }
};
