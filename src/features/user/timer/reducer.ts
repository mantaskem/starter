import { START_TIME, SET_MINUTES } from './constants';
import { createReducer } from 'utils/redux';
import { combineReducers } from 'redux';

export interface TimerState {
  totalMs: number;
  date: Date;
}

const defaultState: TimerState = {
  date: new Date(),
  totalMs: 0
};

const dateReducer = createReducer(
  {
    [START_TIME]: (_, action) => action.payload
  },
  defaultState.date
);

const msReducer = createReducer(
  {
    [SET_MINUTES]: (_, action) => action.payload
  },
  defaultState.totalMs
);

export default combineReducers({ date: dateReducer, totalMs: msReducer });
