import { START_TIME, SET_MINUTES } from 'features/user/timer/constants';
import { createActionCreator } from 'utils/redux';

export const startTime = createActionCreator<Date>(START_TIME);
export const setMiliseconds = createActionCreator<number>(SET_MINUTES);
