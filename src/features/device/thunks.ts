import { AppState } from 'store/reducer';
import { setBreakpoint, setOnline } from './actions';
import { getBreakpoint } from './breakpoint';
import { Dispatch } from 'redux';
import { Breakpoint } from './constants';

export const detectBreakpoint = () => (dispatch: Dispatch<AppState>, getState: () => AppState) => {
  const breakpoint: Breakpoint = getBreakpoint();

  if (getState().device.breakpoint === breakpoint) {
    return;
  }

  dispatch(setBreakpoint(breakpoint));
};

export const detectNetworkStatus = () => (dispatch: Dispatch<AppState>) => {
  const online = window.navigator.onLine;

  dispatch(setOnline(online));
};
