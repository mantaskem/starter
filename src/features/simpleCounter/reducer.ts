import { INCREASE } from './constants';
import { createReducer } from 'utils/redux';
import { combineReducers } from 'redux';

export interface CounterState {
  counter: number;
}

const defaultState: CounterState = {
  counter: 0
};

const countReducer = createReducer(
  {
    [INCREASE]: (state, action) => state + action.payload
  },
  defaultState.counter
);

export default combineReducers({ counter: countReducer });
