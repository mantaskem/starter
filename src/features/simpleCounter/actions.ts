import { INCREASE } from 'features/simpleCounter/constants';
import { createActionCreator } from 'utils/redux';

export const increase = createActionCreator<number>(INCREASE);
