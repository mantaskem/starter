import { connect } from 'react-redux';
import ListPage from 'app/list/Component';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListPage);
